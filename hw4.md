## 数据获取
使用flickr search api， 使用ajax从图片分享网站获取符合主题要求的json数据。
## 瀑布流
利用Masonry.js 实现类似于pinterest 设计的网页，每个卡片及对应的文本输入框均为bootstrap风格，并且实现了不同尺寸的图片在动态添加到页面中时仍然可以实现合理的布局，不会出现大面积的留白，卡片错落有致。
卡片中除了图片之外，还有对应的伪评论，同样为从flickr public feed API 获得的数据。
利用jQuery 插件 fancybox实现点击图片浮现大图的效果，并且在大图的浮现状态之下，可以点击图片区域左右两边位置的箭头来浏览上一张或下一张照片，实现gallery的效果。而且点击卡片后浮现的大图的最下方有相关的伪评论。
## 模版引擎
使用underscore.js 的模版引擎实现从flickr API获得的json数据到html的渲染，简化了开发过程中可能遇到的繁琐代码，并且提高了渲染的效率。
## 异步加载
监听window.scroll 事件，当滚动到页面底部时，触发加载图片的异步函数。
## 设计
网页在设计上结合了pinterest 风格的Masonry 和 twitter 风格的bootstrap ,现代简洁大方，卡片式，扁平化的风格体现出科技感与亲和性。
## 发布地址
[link](http://wenfahu.github.io/works/dashboard/index.html)
